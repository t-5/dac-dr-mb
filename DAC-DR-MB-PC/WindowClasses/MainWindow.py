import os
import sys
from json import JSONDecodeError, dumps, loads
from tempfile import mkstemp
from time import sleep, time
import serial

from PyQt5 import QtCore
from PyQt5.QtCore import QSettings, Qt, pyqtSignal, QTimer, QCoreApplication
from PyQt5.QtGui import QPixmap, QCursor
from PyQt5.QtWidgets import QCheckBox, QComboBox, QGridLayout, QLineEdit, QMainWindow, QLabel, QMessageBox, \
    QInputDialog, \
    QPushButton, QSizePolicy, QSpacerItem, QSpinBox
from designer_qt5.Ui_MainWindow import Ui_MainWindow

from CustomWidgets.VolumeKnob import VolumeKnob
from WindowClasses.AboutDialog import AboutDialog
from helpers.SerialThread import SerialThread
from helpers.HttpServerThread import HttpServerThread
from helpers.styles import STYLESHEET_MUTEBUTTONS
from Hardware import AVAILABLE_DAC_CLASSES


class MainWindow(QMainWindow, Ui_MainWindow):

    MAX_DACS = 14

    KNOB_HEIGHT_PERCENT = 85
    INDICATOR_WIDTH = 10
    TABWIDGET_TABBAR_HEIGHT = 24

    signal_logSerial = pyqtSignal(str)
    signal_mute = pyqtSignal()
    signal_powerGoodStateChanged = pyqtSignal(int)
    signal_setVolume = pyqtSignal(int)
    signal_serialError = pyqtSignal(str)
    signal_stepVolume = pyqtSignal(int, int)
    signal_volumeDown = pyqtSignal()
    signal_volumeUp = pyqtSignal()


    def __init__(self):
        super(MainWindow, self).__init__()
        self._debug = len(sys.argv) > 1 and sys.argv[1] == "debug"
        self.setupUi(self)
        ## module import checks
        for modulename in ("PyQt5",):
            self._importCheck(modulename)
        # condition to wait on volume change signals
        self._condition_setVolume = QtCore.QWaitCondition()
        # state variables:
        self._dirty = False
        self._lastInputTimestamp = 0
        self._rampupInterrupt = False
        self._volume = 0                               # 0: -∞ dB, 1 to 64: -63 dB to 0 dB
        self._swVolumes = [0] * (self.MAX_DACS // 2)   # 0: -∞ dB, 1 to 64: -63 dB to 0 dB
        self._dacs = [None] * self.MAX_DACS            # DAC instances
        self._muteStates = [0] * self.MAX_DACS         # 0: muted, 1: unmuted
        self._powerState = 0                           # 0: bad, 1: good
        self._minVolume = 0
        self._comport = ""
        self._doMuteRelayRestore = False
        self._doVolumeRampUp = False
        self._presets = {}
        # log widget timer
        self._logWidgetTimer = QTimer(self)
        self._logWidgetTimer.setInterval(500)
        self._logWidgetTimer.start()
        self._lastLogActivity = time()
        # settings and initialization and init com port
        self._settings = QSettings("DacDrMbPC", "DacDrMbPC")
        self._serial = None
        self._readIniSettings()
        self._initComport()
        self._serialThread = SerialThread(parent=self, serial=self._serial)
        self._serialThread.start()
        # set up the tab widget
        self._readDacs()
        self._setupTabWidgets()
        # set up and start webserver
        self._mutex = QtCore.QMutex()
        self._httpd = HttpServerThread(parent=self,
                                       mtx=self._mutex,
                                       cond_volume=self._condition_setVolume)
        self._httpd.start()
        # setup write ini settings timer
        self._writeInitSettingsTimer = QTimer()
        self._writeInitSettingsTimer.setInterval(10000)
        self._writeInitSettingsTimer.start()
        # restore stuff
        self._restoreTimer = QTimer()
        self._restoreTimer.setInterval(1000)
        self._restoreTimer.start()
        # finally connect all signals
        self._connectSignals()


    def closeEvent(self, evt):
        """ do stuff necessary before closing the main window """
        self._rampupInterrupt = True
        if self._serialThread:
            self._serialThread.stop()
            while self._serialThread.isRunning():
                sleep(0.1)
        if self._httpd:
            self._httpd.stop()
            while self._httpd.isRunning():
                sleep(0.1)
        self._writeIniSettings()


    def getMinVolume(self):
        return self._minVolume


    def getMuteStates(self):
        return self._muteStates


    def getPowerState(self):
        return self._powerState


    def getVolume(self):
        return self._volume


    def onExit(self):
        self.close()


    def onIndividualGainChanged(self):
        self._dirty = True
        self._inputHappened()
        miv = self.spinBoxGain1.value()
        miv = min(miv, self.spinBoxGain3.value())
        miv = min(miv, self.spinBoxGain5.value())
        miv = min(miv, self.spinBoxGain7.value())
        miv = min(miv, self.spinBoxGain9.value())
        miv = min(miv, self.spinBoxGain11.value())
        miv = min(miv, self.spinBoxGain13.value())
        self._minVolume = -miv + 1
        self.spinBoxOverallGain.blockSignals(True)
        self.spinBoxOverallGain.setMinimum(-64 + self._minVolume)
        self.spinBoxOverallGain.blockSignals(False)
        self._volumeKnob.setMinValue(self._minVolume)
        if self._volume == 0:
            return
        if self._volume < self._minVolume:
            self._setVolume(self._minVolume)
        else:
            self._setVolume(self._volume)


    def onLogSerial(self, s):
        if not self.checkBoxSerialActive.checkState():
            return
        self.textEditSerialLog.appendPlainText(s)
        self.textEditSerialLog.verticalScrollBar().setValue(self.textEditSerialLog.verticalScrollBar().maximum())



    def onMute(self):
        self._inputHappened()
        self._setVolume(0)


    def onMuteButtonChecked(self):
        newStates = []
        for i in range(1, self.MAX_DACS + 1):
            checked = getattr(self, "buttonMuteCh%d" % i).isChecked()
            if checked:
                newStates.append(0)
            else:
                newStates.append(1)
        self._setMuteStates(newStates)


    def onOverallGainChanged(self):
        self._inputHappened()
        self._setVolume(64 + self.spinBoxOverallGain.value())


    def onPowerGoodStateChanged(self, state):
        self._powerState = state
        if state:
            # set led labels
            self.labelPowerLed.setPixmap(QPixmap(":/VolumeKnob/led_green.png"))
            self.labelPowerStatus.setText("Good")
            self.labelPowerStatus.setStyleSheet("color: #00ff00;")
            self._log("PG state changed to GOOD.")
            self._log("Programming DACs...")
            for idx, dac in enumerate(self._dacs):
                if dac is not None:
                    # noinspection PyUnresolvedReferences
                    dac.init()
                    self._log("  DAC CH %d" % (idx + 1))
            #  unmute output relays and DACs
            if self._doMuteRelayRestore:
                self._setHwMuteStates()
                self._updateMuteLEDs(self._muteStates)
                self._log("Unmute relays.")
        else:
            #  mute output relays and DACs
            self._setHwMuteStates(allOff=True)
            self._updateMuteLEDs([0] * self.MAX_DACS)
            # set led labels
            self.labelPowerLed.setPixmap(QPixmap(":/VolumeKnob/led_red.png"))
            self.labelPowerStatus.setText("Bad")
            self.labelPowerStatus.setStyleSheet("color: #ff0000;")
            self._log("PG state changed to BAD.")
            self._log("Muted all relays and DACs.")


    def onReadIniSettings(self):
        self._readIniSettings()


    def onSerialError(self, msg):
        return
        dlg = QMessageBox(self)
        dlg.setStyleSheet(self.styleSheet())
        dlg.critical(self, "Serial pioError",
                     "There was an error communicating with the arduino controller over serial port:\n\n"
                     + msg
                     + "Please choose port to try to reconnect to...")
        self._initComport()


    def onSerialSend(self):
        self._serial.write(self.lineEditSerialInput.text().encode("ascii"))
        self.lineEditSerialInput.setText("")


    def onSetVolume(self, volume):
        self._inputHappened()
        if volume == 0:
            self._setVolume(0)
            return
        if volume < self._minVolume:
            volume = self._minVolume
        self._setVolume(volume)


    def onStepVolume(self, direction, steps):
        if direction:
            self.onVolumeUp(steps)
        else:
            self.onVolumeDown(steps)


    @staticmethod
    def onShowAboutDialog(_):
        """ show about dialog """
        dlg = AboutDialog()
        dlg.exec_()


    def onVolumeDown(self, steps=1):
        self._inputHappened()
        if self._volume == 0:
            return
        if self._volume <= self._minVolume:
            self._setVolume(0)
        else:
            self._setVolume(max(self._minVolume, self._volume - steps))


    def onVolumeKnobChanged(self):
        self._inputHappened()
        self._setVolume(self._volumeKnob.value())


    def onVolumeKnobClicked(self):
        self._inputHappened()


    def onVolumeUp(self, steps=1):
        self._inputHappened()
        if self._volume >= 64:
            return
        if self._volume == 0 and self._minVolume > 0:
            self._setVolume(self._minVolume)
        else:
            self._setVolume(min(64, self._volume + steps))


    def resizeEvent(self, event):
        if self.isHidden() or self.tabWidget.currentIndex() != 0:
            return
        h = int(round(self.volumeTab.height() * self.KNOB_HEIGHT_PERCENT / 100))
        x = int(round((self.width() - h) / 2))
        y = int(round((self.volumeTab.height() - h) / 2)) - self.TABWIDGET_TABBAR_HEIGHT
        self._volumeKnob.move(x, y)
        self._volumeKnob.setFixedSize(h, h)


    def _addDAC(self):
        num = self.sender().number
        cls, ok = QInputDialog.getItem(self, "Select DAC class", "", AVAILABLE_DAC_CLASSES.keys())
        if not ok:
            return
        dac = AVAILABLE_DAC_CLASSES[cls](self._serial)
        self._dacs[num - 1] = dac
        grid = getattr(self, "dacSettingsGrid%d" % num)
        for widget in grid.widgets:
            widget.hide()
            widget.deleteLater()
        grid.removeItem(grid.spacer)
        self._setupDacSettings(grid, dac, num)


    def _calculateAndUpdateSwVolumes(self):
        for i in range(0, self.MAX_DACS // 2):
            self._swVolumes[i] = max(0, self._volume + getattr(self, "spinBoxGain%d" % (i * 2 + 1)).value())
            label = getattr(self, "labelGain%d" % (i * 2 + 1))
            label.setText(self._swVolumes[i] == 0 and "-∞ dB" or f'{self._swVolumes[i] - 64} dB')


    def _checkBoxRestoreVolumeChanged(self):
        self._doVolumeRampUp = self.checkBoxRestoreVolume.isChecked()


    def _checkBoxRestoreMuteRelaysChanged(self):
        self._doMuteRelayRestore = self.checkBoxRestoreMuteRelays.isChecked()


    def _checkLogWidget(self):
        if self._lastLogActivity + 10 < time():
            self.textEditLog.hide()


    # noinspection PyUnresolvedReferences
    def _connectSignals(self):
        self.spinBoxGain1.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxGain3.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxGain5.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxGain7.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxGain9.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxGain11.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxGain13.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxOverallGain.valueChanged.connect(self.onOverallGainChanged)
        self.signal_logSerial.connect(self.onLogSerial)
        self.signal_mute.connect(self.onMute)
        self.signal_powerGoodStateChanged.connect(self.onPowerGoodStateChanged)
        self.signal_serialError.connect(self.onSerialError)
        self.signal_setVolume.connect(self.onSetVolume)
        self.signal_stepVolume.connect(self.onStepVolume)
        self.signal_volumeDown.connect(self.onVolumeDown)
        self.signal_volumeUp.connect(self.onVolumeUp)
        self.tabWidget.currentChanged.connect(self._inputHappened)
        self._writeInitSettingsTimer.timeout.connect(self._writeInitSettingsTimerHandler)
        self._logoLabel.mousePressEvent = self.onShowAboutDialog
        self._volumeKnob.valueChanged.connect(self.onVolumeKnobChanged)
        self._volumeKnob.clicked.connect(self.onVolumeKnobClicked)
        self._volumeKnob.doubleClicked.connect(self.onMute)
        for i in range(1, self.MAX_DACS + 1):
            getattr(self, "buttonMuteCh%d" % i).toggled.connect(self.onMuteButtonChecked)
        self.pushButtonReconnect.clicked.connect(self._initComport)
        self.checkBoxRestoreVolume.stateChanged.connect(self._checkBoxRestoreVolumeChanged)
        self.checkBoxRestoreMuteRelays.stateChanged.connect(self._checkBoxRestoreMuteRelaysChanged)
        self._logWidgetTimer.timeout.connect(self._checkLogWidget)
        self._restoreTimer.timeout.connect(self._restoreStates)
        self.pushButtonAddPreset.clicked.connect(self._onAddPreset)
        self.pushButtonDeletePreset.clicked.connect(self._onDeletePreset)
        self.pushButtonApplyPreset.clicked.connect(self._onApplyPreset)
        self.pushButtonSerialSend.clicked.connect(self.onSerialSend)
        self.lineEditSerialInput.returnPressed.connect(self.onSerialSend)


    def _dacSettingChanged(self):
        sender = self.sender()
        dac = sender.dac
        attributeName = sender.attributeName
        inputType = sender.inputType
        if inputType == "bool":
            setattr(dac, attributeName, sender.isChecked())
        elif inputType == "int":
            setattr(dac, attributeName, sender.value())
        elif inputType == "select":
            setattr(dac, attributeName, sender.currentData())


    def _gpioWrite(self, value):
        self._serial.write(b"<PC>gpio%s</PC>\n" % value)


    def _importCheck(self, modulename):
        try:
            __import__(modulename)
        except ImportError:
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            msg = "WARNING:\n"
            msg += "  Could not find python module '%s'.\n" % modulename
            msg += "  Should we try to install it for you?\n"
            msg += "  (This may take a few seconds or minutes...)"
            result = dlg.warning(self, "Dependencies warning", msg, QMessageBox.Yes | QMessageBox.No)
            if result == QMessageBox.Yes:
                ret = os.system("pip3 install %s" % modulename)
                if ret != 0:
                    msg = "ERROR: module '%s' not found.\n" % modulename
                    msg += "  Manual installation: 'pip3 install %s'" % modulename
                    dlg.critical(self, "ERROR loading dependencies!", msg)
                    print(msg, file=sys.stderr)
                    sys.exit(1)
                else:
                    msg = "SUCCESS:\n"
                    msg += "  The module '%s' was successfully installed.\n" % modulename
                    msg += "  The program will be restarted now..."
                    dlg.information(self, "Need to restart...", msg, QMessageBox.Ok)
                    fd, fn = mkstemp()
                    os.close(fd)
                    os.chmod(fn, 0o700)
                    f = open(fn, "w")
                    f.write("#!/bin/sh\nsleep 2\n/usr/bin/dac-dr-mb-pc\nrm -f %s\n" % fn)
                    f.close()
                    os.system("%s &" % fn)
                    sys.exit(0)
            else:
                sys.exit(1)


    def _initComport(self):
        self._comport = self.lineEditComPort.text()
        ok = True
        connected = False
        while ok and not connected:
            try:
                self._serial = serial.Serial(self._comport, timeout=1)
                self._serial.write(b"<PC>pgst</PC>\n")
                response = self._serial.read(1000)
                print(response)
                if b"<MB>POWER" in response or self._debug:
                    connected = True
                else:
                    raise ValueError("Invalid response from DAC-DR-MB: '%s'" % response)
            except (serial.serialutil.SerialException, ValueError) as e:
                title = "COMPORT ERROR"
                error = str(e)
                msg = "Could not connect to com port '':\n%s\n\nPlease enter a valid com port or press Cancel to exit the program" % error
                self._comport, ok = QInputDialog.getText(self, title, msg, QLineEdit.Normal, self._comport)
        if not ok:
            sys.exit(1)
        self.lineEditComPort.setText(self._comport)


    def _inputHappened(self):
        self._rampupInterrupt = True


    def _log(self, s):
        self._lastLogActivity = time()
        self.textEditLog.show()
        self.textEditLog.append(s)
        self.textEditLog.verticalScrollBar().setValue(self.textEditLog.verticalScrollBar().maximum())


    def _onAddPreset(self):
        name, ok = QInputDialog.getText(self, "Add Preset", "Preset description                                                                ")
        if not ok:
            return
        if name in self._presets:
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            msg = "Preset name already exists, preset was not added."
            return dlg.critical(self, "ERROR", msg, QMessageBox.OK)
        preset = []
        for dac in self._dacs:
            # noinspection PyUnresolvedReferences
            preset.append(dac and dac.json() or None)
        self.listWidgetPresets.addItem(name)
        self._presets[name] = preset


    def _onDeletePreset(self):
        listItems = self.listWidgetPresets.selectedItems()
        if not listItems:
            return
        for item in listItems:
            del self._presets[item.text()]
            self.listWidgetPresets.takeItem(self.listWidgetPresets.row(item))


    def _onApplyPreset(self):
        # remove all existing dacs
        for num in range(1, self.MAX_DACS + 1):
            self._removeDac(num)
        # add dacs from preset
        for idx, json in enumerate(self._presets[self.listWidgetPresets.currentItem().text()]):
            dac = self._readDac(json)
            self._dacs[idx] = dac
            if dac is not None:
                grid = getattr(self, "dacSettingsGrid%i" % (idx + 1))
                self._setupDacSettings(grid, dac, idx + 1)
                if self._powerState:
                    dac.init()


    def _onReinitDac(self):
        if not self._powerState:
            return
        # noinspection PyUnresolvedReferences
        self._dacs[self.sender().number - 1].init()


    def _onRemoveDac(self):
        self._removeDac(self.sender().number)


    def _readDac(self, json):
        try:
            data = loads(json)
            if data:
                return AVAILABLE_DAC_CLASSES[data["className"]](self._serial, json)
        except (JSONDecodeError, TypeError):
            pass


    def _readDacs(self):
        s = self._settings
        for i in range(1, self.MAX_DACS + 1):
            json = s.value("dac%d" % i, "")
            self._dacs[i - 1] = self._readDac(json)


    def _readIniSettings(self):
        """ read ini settings """
        s = self._settings
        # state restore options
        self._doMuteRelayRestore = s.value("doMuteRelayRestore", "false") == "true"
        self._doVolumeRampUp = s.value("doVolumeRampUp", "false") == "true"
        # mute states
        if self._doMuteRelayRestore:
            try:
                v = s.value("muteStates", [0] * self.MAX_DACS)
                if not isinstance(v, list) or len(v) != 14:
                    v = [0] * self.MAX_DACS
                v = list(map(lambda x: int(x), v))
            except ValueError:
                v = [0] * self.MAX_DACS
            self._muteStates = v
            for i, val in enumerate(v):
                if val < 0 or val > 1:
                    val = 0
                getattr(self, "buttonMuteCh%d" % (i + 1)).setChecked(not bool(val))
        # individual attenuation offsets
        try:
            v = s.value("attenuationOffsets", [0] * (self.MAX_DACS // 2))
            if not isinstance(v, list) or len(v) != 7:
                v = [0] * (self.MAX_DACS // 2)
            v = list(map(lambda x: int(x), v))
        except ValueError:
            v = [0] * (self.MAX_DACS // 2)
        self._swVolumes = v
        for i, val in enumerate(v):
            if val < -63 or val > 0:
                val = 0
            getattr(self, "spinBoxGain%d" % (i * 2 + 1)).setValue(val)
        # com port
        self._comport = s.value("comport", "/dev/ttyACM0")
        self.lineEditComPort.setText(self._comport)
        # presets
        try:
            self._presets = loads(s.value("presets", b""))
        except JSONDecodeError:
            self._presets = {}
        # main window geometry
        geo = s.value("main/mainWindowGeo")
        if geo is not None:
            self.restoreGeometry(geo)
        state = s.value("main/mainWindowState")
        if state is not None:
            self.restoreState(state)


    def _reinitDac(self, dac):
        if self._powerState:
            dac.init()


    def _reinitDacs(self):
        for dac in self._dacs:
            self._reinitDac(dac)


    def _removeDac(self, num):
        if self._dacs[num - 1] is None:
            return
        self._dacs[num - 1] = None
        grid = getattr(self, "dacSettingsGrid%d" % num)
        for widget in getattr(grid, "widgets", []):
            widget.hide()
            widget.deleteLater()
            grid.removeWidget(widget)
        if getattr(grid, "spacer", None):
            grid.removeItem(grid.spacer)
        tab = getattr(self, "dacsTab%i" % num)
        addButton = QPushButton("Add DAC", tab)
        addButton.number = num
        # noinspection PyUnresolvedReferences
        addButton.clicked.connect(self._addDAC)
        addButton.setGeometry(5, 5, 100, 40)
        grid.addWidget(addButton, 1, 1)
        grid.widgets = [addButton]
        setattr(self, "addDacButton%i" % num, addButton)
        spacer = QSpacerItem(1, 1, QSizePolicy.Expanding, QSizePolicy.Expanding)
        grid.addItem(spacer, 2, 2)
        grid.spacer = spacer


    def _restoreStates(self):
        # request PG status
        self._serial.write(b"<PC>pgst</PC>\n")
        if not self._doMuteRelayRestore and not self._doVolumeRampUp:
            self._restoreTimer.stop()
            return
        if self._powerState:
            self._log("Ramping up volume...")
            self._restoreTimer.stop()
            # volume
            try:
                volume = int(self._settings.value("volume", 0))
                if volume != 0 and (volume < self._minVolume or volume > 64):
                    volume = 0
            except ValueError:
                volume = 0
            for v in range(self._minVolume, volume + 1):
                if self._rampupInterrupt:
                    break
                self._setVolume(v)
                for i in range(0, 20):  # sleep 200ms and process GUI events inbetween
                    QCoreApplication.processEvents()
                    sleep(0.01)
            self._log("DONE.")


    def _setHwMuteStates(self, allOff=False):
        if allOff:
            self._gpioWrite(b"0000000000000000")
            for idx, dac in enumerate(self._dacs):
                if dac is not None:
                    # noinspection PyUnresolvedReferences
                    dac.setVolume(0)
        else:
            value = b""
            for mute in self._muteStates:
                value += mute and b"1" or b"0"
            value += b"00"
            self._gpioWrite(value)
            for idx, dac in enumerate(self._dacs):
                if dac is not None:
                    # noinspection PyUnresolvedReferences
                    dac.setVolume(self._swVolumes[idx])


    def _setMuteStates(self, muteStates):
        self._dirty = True
        self._muteStates = muteStates
        if not self._powerState:
            return
        # update hardware mute states:
        self._setHwMuteStates()
        self._updateMuteLEDs(self._muteStates)


    def _setupDacSettings(self, grid, dac, num):
        grid.widgets = []
        label = QLabel(dac.__class__.__name__, self)
        grid.widgets.append(label)
        label.setStyleSheet("font-weight: bold;")
        grid.addWidget(label, 1, 1)
        row = 2
        # noinspection PyUnresolvedReferences
        for setting in self._dacs[num - 1].settings():
            label = QLabel(setting["description"], self)
            grid.widgets.append(label)
            grid.addWidget(label, row, 1)
            inputItem = None
            if setting["input"] == "bool":
                inputItem = QCheckBox("", self)
                inputItem.setChecked(getattr(dac, setting["attribute"]))
                # noinspection PyUnresolvedReferences
                inputItem.stateChanged.connect(self._dacSettingChanged)
            elif setting["input"] == "int":
                inputItem = QSpinBox(self)
                inputItem.setMinimum(setting["min"])
                inputItem.setMaximum(setting["max"])
                inputItem.setValue(getattr(dac, setting["attribute"]))
                # noinspection PyUnresolvedReferences
                inputItem.valueChanged.connect(self._dacSettingChanged)
            elif setting["input"] == "select":
                inputItem = QComboBox(self)
                idx = 0
                currentIdx = 0
                dacValue = getattr(dac, setting["attribute"])
                for value, text in setting["options"].items():
                    inputItem.addItem(text, value)
                    if value == dacValue:
                        currentIdx = idx
                    idx += 1
                inputItem.setCurrentIndex(currentIdx)
                # noinspection PyUnresolvedReferences
                inputItem.currentIndexChanged.connect(self._dacSettingChanged)
            grid.widgets.append(inputItem)
            inputItem.dac = dac
            inputItem.attributeName = setting["attribute"]
            inputItem.inputType = setting["input"]
            grid.addWidget(inputItem, row, 2)
            row += 1

        removeDacButton = QPushButton("Remove DAC", self)
        removeDacButton.number = num
        setattr(self, "removeDacButton%d" % num, removeDacButton)
        # noinspection PyUnresolvedReferences
        removeDacButton.clicked.connect(self._onRemoveDac)
        grid.addWidget(removeDacButton, row, 1)
        grid.widgets.append(removeDacButton)

        reinitDacButton = QPushButton("Reinitialize DAC", self)
        reinitDacButton.number = num
        setattr(self, "reinitDacButton%d" % num, reinitDacButton)
        # noinspection PyUnresolvedReferences
        reinitDacButton.clicked.connect(self._onReinitDac)
        grid.addWidget(reinitDacButton, row, 2)
        grid.widgets.append(reinitDacButton)

        spacer = QSpacerItem(1, 1, QSizePolicy.Expanding, QSizePolicy.Expanding)
        grid.addItem(spacer, row + 1, 2)
        grid.spacer = spacer


    def _setupTabWidgets(self):
        self.tabWidget.setCurrentIndex(0)
        self.tabWidget.setTabText(0, "Volume Control")
        self.tabWidget.setTabText(1, "Mixer Controls")
        self.tabWidget.setTabText(2, "Options")
        self.tabWidget.setTabText(3, "DACs")
        self.tabWidget.setTabText(4, "Presets")
        self._volumeKnob = VolumeKnob(self.tabWidget.currentWidget(), width=self.INDICATOR_WIDTH, steps=64, value=0)
        self.spinBoxOverallGain.setValue(-63)
        for i in range(1, (self.MAX_DACS // 2) + 1, 2):
            getattr(self, "labelGain%d" % i).setText("-∞ dB")
        # led labels
        self._logoLabel = QLabel(self)
        self._logoLabel.setScaledContents(True)
        self._logoLabel.setPixmap(QPixmap(":/MainWindow/t5_white_on_transparent.png"))
        self._logoLabel.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
        self._logoLabel.setFixedSize(38, 26)
        self._logoLabel.move(955, 7)
        self._logoLabel.setCursor(QCursor(Qt.PointingHandCursor))
        # set stylesheets for mute buttons
        for i in range(1, self.MAX_DACS + 1):
            getattr(self, "buttonMuteCh%d" % i).setStyleSheet(STYLESHEET_MUTEBUTTONS)
        # options tab
        self.lineEditComPort.setText(self._comport)
        self.checkBoxRestoreVolume.setChecked(self._doVolumeRampUp)
        self.checkBoxRestoreMuteRelays.setChecked(self._doMuteRelayRestore)
        # DACs tab
        for i in range(1, self.MAX_DACS + 1):
            tab = getattr(self, "dacsTab%i" % i)
            grid = QGridLayout(tab)
            setattr(self, "dacSettingsGrid%d" % i, grid)
            self.dacsTabWidget.setTabText(i - 1, "CH %d" % i)
            if self._dacs[i - 1] is None:
                addButton = QPushButton("Add DAC", tab)
                addButton.number = i
                # noinspection PyUnresolvedReferences
                addButton.clicked.connect(self._addDAC)
                setattr(self, "addDacButton%i" % i, addButton)
                grid.widgets = [addButton]
                spacer = QSpacerItem(1, 1, QSizePolicy.Expanding, QSizePolicy.Expanding)
                grid.addItem(spacer, 2, 2)
                grid.spacer = spacer
            else:
                # noinspection PyTypeChecker
                self._setupDacSettings(grid, self._dacs[i - 1], i)
        # presets tab
        for name in self._presets:
            self.listWidgetPresets.addItem(name)


    def _setVolume(self, volume):
        if volume < 0 or volume > 64:
            raise ValueError("volume out of bounds: %r" % volume)
        print(f"new volume: {volume}")
        self._dirty = True
        self._volume = volume
        # update volume knob
        self._volumeKnob.blockSignals(True)
        self._volumeKnob.setValue(volume)
        self._volumeKnob.blockSignals(False)
        # update overall gain spinbox
        self.spinBoxOverallGain.blockSignals(True)
        self.spinBoxOverallGain.setValue(volume - 64)
        self.spinBoxOverallGain.blockSignals(False)
        # calculate individual sw volumes and update labels accordingly
        self._calculateAndUpdateSwVolumes()
        if self._powerState:
            # calculate and set individual sw volumes
            self._setVolumes()
        # finally wake the set volume condition
        self._condition_setVolume.wakeAll()


    def _setVolumes(self):
        for idx, dac in enumerate(self._dacs):
            # noinspection PyUnresolvedReferences
            if dac:
                if self._debug:
                    print("set hw volume on DAC %d to %d" % (idx, self._swVolumes[idx]))
                # noinspection PyUnresolvedReferences
                dac.setVolume(self._swVolumes[idx])


    def _updateMuteLEDs(self, states):
        for idx, state in enumerate(states):
            res = state and ":/VolumeKnob/led_green.png" or ":/VolumeKnob/led_red.png"
            getattr(self, "labelLedCh%d" % (idx + 1)).setPixmap(QPixmap(res))


    def _writeIniSettings(self):
        """ write ini settings """
        s = self._settings
        s.setValue("muteStates", self._muteStates)
        s.setValue("minVolume", self._minVolume)
        s.setValue("volume", self._volume)
        s.setValue("attenuationOffsets", [
            self.spinBoxGain1.value(),
            self.spinBoxGain3.value(),
            self.spinBoxGain5.value(),
            self.spinBoxGain7.value(),
            self.spinBoxGain9.value(),
            self.spinBoxGain11.value(),
            self.spinBoxGain13.value(),
        ])
        s.setValue("comport", self._comport)
        self._dirty = False
        # DACs
        for i in range(1, self.MAX_DACS + 1):
            dac = self._dacs[i - 1]
            if dac:
                # noinspection PyUnresolvedReferences
                s.setValue("dac%d" % i, dac.json())
            else:
                s.setValue("dac%d" % i, "")
        # state restore options
        s.setValue("doMuteRelayRestore", self._doMuteRelayRestore)
        s.setValue("doVolumeRampUp", self._doVolumeRampUp)
        # presets
        s.setValue("presets", dumps(self._presets))
        # window geometry
        s.setValue("main/mainWindowGeo", self.saveGeometry())
        s.setValue("main/mainWindowState", self.saveState())


    def _writeInitSettingsTimerHandler(self):
        if self._dirty:
            self._writeIniSettings()
