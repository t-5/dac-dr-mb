from PyQt5 import QtCore
from serial import SerialException


class SerialThread(QtCore.QThread):

    def __init__(self, parent=None, serial=None):
        super().__init__(parent=parent)
        self._running = False
        self._serial = serial
        self._buffer = b""


    def run(self):
        self._running = True
        while self._running:
            try:
                b = self._serial.read(1)
            except SerialException as e:
                self._emitToMainAppOneArg("signal_serialError", str(e))
            if b:
                self._buffer += b
                print(b.decode("ascii"), end="")
            if b == b'>':
                self._emitToMainAppOneArg("signal_logSerial", self._buffer.decode("ascii"))
            if b"<MB>POWER_BAD</MB>" in self._buffer:
                self._emitToMainAppOneArg("signal_powerGoodStateChanged", 0)
                self._buffer = b""
            elif b"<MB>POWER_GOOD</MB>" in self._buffer:
                self._emitToMainAppOneArg("signal_powerGoodStateChanged", 1)
                self._buffer = b""
            elif b"<MB>MUTE</MB>" in self._buffer:
                self._emitToMainAppNoArgs("signal_mute")
                self._buffer = b""
            elif b"<MB>VOLUME_DOWN</MB>" in self._buffer:
                self._emitToMainAppNoArgs("signal_volumeDown")
                self._buffer = b""
            elif b"<MB>VOLUME_UP</MB>" in self._buffer:
                self._emitToMainAppNoArgs("signal_volumeUp")
                self._buffer = b""


    def stop(self):
        self._running = False
        self.wait()


    def _emitToMainAppNoArgs(self, signal):
        qsignal = getattr(self.parent(), signal)
        qsignal.emit()


    def _emitToMainAppOneArg(self, signal, arg):
        qsignal = getattr(self.parent(), signal)
        qsignal.emit(arg)
