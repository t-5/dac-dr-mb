import json
from http.server import SimpleHTTPRequestHandler

from helpers.constants import WEBSERVER_HELPTEXT, WEBSERVER_501_TEXT


class HttpServerHandler(SimpleHTTPRequestHandler):

    def do_GET(self):
        """ handle get responses according their path """
        if self.path == "/":
            status, ret = 200, WEBSERVER_HELPTEXT
        elif self.path == "/alive":
            status, ret = 200, "OK"
        elif self.path == "/get_volume":
            status, ret = self._get_volume()
        elif self.path == "/get_status":
            status, ret = self._get_status()
        elif self.path.startswith("/set_volume"):
            status, ret = self._set_volume()
        elif self.path.startswith("/volume_down"):
            status, ret = self._volume_down()
        elif self.path.startswith("/volume_up"):
            status, ret = self._volume_up()
        else:
            status, ret = 500, "ERROR 500: Unknown path. See / for documentation."
        self.send_response(status)
        self.send_header('Content-Type', 'text/plain')
        self.end_headers()
        self.wfile.write(bytes(ret, "utf8"))


    def _emit(self, signal):
        """ let the server thread emit a signal without arguments """
        # noinspection PyUnresolvedReferences
        self.server.thread.emitToMainAppNoArgs(signal)


    def _emitOneArg(self, signal, arg):
        """ let the server thread emit a signal with one argument """
        # noinspection PyUnresolvedReferences
        self.server.thread.emitToMainAppOneArg(signal, arg)


    def _get_status(self):
        # noinspection PyUnresolvedReferences
        ret = {'muteStates': self.server.thread.parent().getMuteStates(),
               'powerState': self.server.thread.parent().getPowerState(),
               'minVolume': self.server.thread.parent().getMinVolume(),
               'volume': self.server.thread.parent().getVolume()}
        return 200, json.dumps(ret)


    def _get_volume(self):
        """ return random flag """
        # noinspection PyUnresolvedReferences
        return 200, str(self.server.thread.parent().getVolume())


    def _set_volume(self):
        """ set volume """
        # noinspection PyUnresolvedReferences
        self.server.thread.lockMutex()
        if not "?volume=" in self.path:
            return 501, WEBSERVER_501_TEXT
        ignore, arg = self.path.split("?volume=")
        try:
            volume = max(0, min(64, int(arg)))
        except ValueError:
            return 501, WEBSERVER_501_TEXT
        try:
            self._emitOneArg("signal_setVolume", volume)
            # noinspection PyUnresolvedReferences
            self.server.thread.waitForSetVolume()
        finally:
            # noinspection PyUnresolvedReferences
            self.server.thread.unlockMutex()
        return 200, "OK"


    def _volume_down(self):
        """ decrease volume by 1dB """
        # noinspection PyUnresolvedReferences
        self.server.thread.lockMutex()
        try:
            self._emit("signal_volumeDown")
            # noinspection PyUnresolvedReferences
            self.server.thread.waitForSetVolume()
        finally:
            # noinspection PyUnresolvedReferences
            self.server.thread.unlockMutex()
        # noinspection PyUnresolvedReferences
        return 200, "%d" % self.server.thread.parent().getVolume()


    def _volume_up(self):
        """ increase volume by 1dB """
        # noinspection PyUnresolvedReferences
        self.server.thread.lockMutex()
        try:
            self._emit("signal_volumeUp")
            # noinspection PyUnresolvedReferences
            self.server.thread.waitForSetVolume()
        finally:
            # noinspection PyUnresolvedReferences
            self.server.thread.unlockMutex()
        # noinspection PyUnresolvedReferences
        return 200, "%d" % self.server.thread.parent().getVolume()
