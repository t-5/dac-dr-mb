from helpers.constants import *
from helpers.HttpServerHandler import HttpServerHandler
from http.server import HTTPServer
from PyQt5 import QtCore


class HttpServerThread(QtCore.QThread):

    def __init__(self, parent=None,
                 mtx=None,
                 cond_volume=None):
        super().__init__(parent=parent)
        self._mutex = mtx
        self._cond_volume = cond_volume
        self._server = None


    def emitToMainAppNoArgs(self, signal):
        qsignal = getattr(self.parent(), signal)
        qsignal.emit()


    def emitToMainAppOneArg(self, signal, arg):
        qsignal = getattr(self.parent(), signal)
        qsignal.emit(arg)


    def lockMutex(self):
        self._mutex.lock()


    def run(self):
        try:
            self._server = HTTPServer((WEBSERVER_HOST, WEBSERVER_PORT), HttpServerHandler)
            self._server.thread = self
            self._server.serve_forever()
        except OSError:
            print("WARNING: could not bind to address %s:%s, http remote control is disabled."
                  % (WEBSERVER_HOST, WEBSERVER_PORT))


    def stop(self):
        self._server.shutdown()
        self._server.socket.close()
        self.wait()


    def unlockMutex(self):
        self._mutex.unlock()


    def waitForSetVolume(self):
        self._cond_volume.wait(self._mutex)
