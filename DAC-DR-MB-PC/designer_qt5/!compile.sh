#!/bin/bash

for ui in *.ui; do
    echo reading ${ui}...
    py=`echo ${ui} | sed s#.ui#.py#`
    echo writing ${py}...
    pyuic5 ${ui} -o ${py}
done

echo
cd res
pyrcc5 -o ../DacDrMbPC_rc/__init__.py DacDrMbPC.qrc
cd ..
