import struct

from Hardware.AbstractDAC import AbstractDAC


class AbstractES9038Q2M(AbstractDAC):


    CHANNEL_TO_I2C_ADDR = {
        "left": 0x90,
        "right": 0x92,
    }


    FILTER_TYPES = {
        0b111: "Brick wall filter",
        0b110: "Corrected minimum phase fast roll-off filter",
        0b100: "Apodizing fast roll-off filter (default)",
        0b011: "Minimum phase slow roll-off filter",
        0b010: "Minimum phase fast roll-off filter",
        0b001: "Linear phase slow roll-off filter",
        0b000: "Linear phase fast roll-off filter",
    }


    def __init__(self, *args, **kwargs):
        self._filter = 0
        self._thdComp2 = 0
        self._thdComp3 = 0
        super().__init__(*args, **kwargs)


    def i2cAddr(self):
        return self.CHANNEL_TO_I2C_ADDR[self._channel]


    def init(self):
        """ send hardware initialization to DAC """
        if not self._active:
            return
        # reset DAC and set clock gear + oscillator disable
        #   register 0 - SYSTEM_CONFIG, bits 7:4: osc_drv, bits 3:2: clk_gear, bit 0: soft_reset
        self._writeI2cReg(0, 0b11110001)
        # set filter and unmute the DAC outputs
        #   Register 7: Filter Bandwidth and System Mute, bits 7:5
        self._writeI2cReg(7, self._filter << 5)
        # automute and channel mapping
        #   Register 2: Mixing, Serial Data and Automute Configuration
        #   bits 7:6: automute config, bits 3:2 and 1:0 channel selection
        bits = 0b00000000
        if self._channel == "right":
            bits = 0b00000101
        self._writeI2cReg(2, bits)
        # set thd compensation values
        #   registers 23-22 THD C2 and 25-24 THD C3
        c2_16bitSigned = struct.pack('h', self._thdComp2)
        c3_16bitSigned = struct.pack('h', self._thdComp3)
        self._writeI2cReg(23, c2_16bitSigned[0])
        self._writeI2cReg(22, c2_16bitSigned[1])
        self._writeI2cReg(25, c3_16bitSigned[0])
        self._writeI2cReg(24, c3_16bitSigned[1])


    def setVolume(self, volume):
        if not self._active:
            return
        # volume can be:
        # 0: -∞ dB
        # 1 to 64: -63 dB to 0 dB
        if volume == 0:
            value = 0
        else:
            value = 127 + volume * 2
        # registers 15 and 16 - both channels
        self._writeI2cReg(15, value)
        self._writeI2cReg(76, value)


    def settings(self) -> list:
        d = super().settings()
        d.append({
            "attribute": "_filter",
            "description": "Filter",
            "type": int,
            "input": "select",
            "options": self.FILTER_TYPES,
            "default": 0b100,
        })
        d.append({
            "attribute": "_thdComp2",
            "description": "THD Compensation register H2",
            "type": int,
            "input": "int",
            "min": -32768,
            "max": 32767,
            "default": 0,
        })
        d.append({
            "attribute": "_thdComp3",
            "description": "THD Compensation register H3",
            "type": int,
            "input": "int",
            "min": -32768,
            "max": 32767,
            "default": 0,
        })
        return d


    def _jsonData(self):
        data = super()._jsonData()
        data["filter"] = self._filter
        data["thdComp2"] = self._thdComp2
        data["thdComp3"] = self._thdComp3
        return data


    def _parseFromJsonData(self, data):
        super()._parseFromJsonData(data)
        self._filter = data["filter"]
        self._thdComp2 = data["thdComp2"]
        self._thdComp3 = data["thdComp3"]
