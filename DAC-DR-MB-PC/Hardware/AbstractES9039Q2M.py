import struct

from Hardware.AbstractDAC import AbstractDAC


class AbstractES9039Q2M(AbstractDAC):


    CHANNEL_TO_I2C_ADDR = {
        "left": 0x90,
        "right": 0x92,
    }


    FILTER_TYPES = {
        0: "Minimum phase",
        1: "Linear phase apodizing fast roll-off",
        2: "Linear phase fast roll-off",
        3: "Linear phase fast roll-off low ripple",
        4: "Linear phase slow roll-off",
        5: "Minimum phase fast roll-off",
        6: "Minimum phase slow roll-off",
        7: "Minimum phase slow roll-off low dispersion",
    }


    def __init__(self, *args, **kwargs):
        self._filter = 0
        self._thdComp2 = 0
        self._thdComp3 = 0
        super().__init__(*args, **kwargs)


    def i2cAddr(self):
        return self.CHANNEL_TO_I2C_ADDR[self._channel]


    def init(self):
        """ send hardware initialization to DAC """
        if not self._active:
            return
        # reset DAC and disable analog output
        #   register 0 - SYSTEM_CONFIG, bit 7: SOFT_RESET, bit 1: DAC_MODE
        self._writeI2cReg(0, 0b10000000)
        # disable automute
        #   register 123 AUTOMUTE ENABLE
        self._writeI2cReg(123, 0b00000000)
        # set filter
        #   register 88 - FILTER SHAPE
        self._writeI2cReg(88, self._filter)
        # set thd compensation values
        #   registers 94-91 THD C2 and 110-107 THD C3
        c2_16bitSigned = struct.pack('h', self._thdComp2)
        c3_16bitSigned = struct.pack('h', self._thdComp3)
        self._writeI2cReg(94, c2_16bitSigned[0])
        self._writeI2cReg(93, c2_16bitSigned[1])
        self._writeI2cReg(92, c2_16bitSigned[0])
        self._writeI2cReg(91, c2_16bitSigned[1])
        self._writeI2cReg(110, c3_16bitSigned[0])
        self._writeI2cReg(109, c3_16bitSigned[1])
        self._writeI2cReg(108, c3_16bitSigned[0])
        self._writeI2cReg(107, c3_16bitSigned[1])
        # disable calibration resistor
        #   register 34 DATA PATH CONFIG, bit 7 CH2_NSMOD_IN_SEL and bit 6 CAL_RES_ENB
        self._writeI2cReg(34, 0b00000000)
        # set I2S inputs for output channels
        #   registers 64 CH1 SLOT CONFIG and 65 CH2 SLOT CONFIG, bits 7:5 DSD_CH1/2_SOURCE and bits 4:0 PCM_CH1/2_SLOT_SEL
        bits = 0b00000000
        if self._channel == "right":
            bits = 0b00000001
        self._writeI2cReg(64, bits)
        self._writeI2cReg(65, bits)


    def setVolume(self, volume):
        if not self._active:
            return
        # volume can be:
        # 0: -∞ dB
        # 1 to 64: -63 dB to 0 dB
        if volume == 0:
            value = 0
        else:
            value = 127 + volume * 2
        # registers 74 and 75 - both channels
        self._writeI2cReg(74, value)
        self._writeI2cReg(75, value)


    def settings(self) -> list:
        d = super().settings()
        d.append({
            "attribute": "_filter",
            "description": "Filter",
            "type": int,
            "input": "select",
            "options": self.FILTER_TYPES,
            "default": 0,
        })
        d.append({
            "attribute": "_thdComp2",
            "description": "THD Compensation register H2",
            "type": int,
            "input": "int",
            "min": -32768,
            "max": 32767,
            "default": 0,
        })
        d.append({
            "attribute": "_thdComp3",
            "description": "THD Compensation register H3",
            "type": int,
            "input": "int",
            "min": -32768,
            "max": 32767,
            "default": 0,
        })
        return d


    def _jsonData(self):
        data = super()._jsonData()
        data["filter"] = self._filter
        data["thdComp2"] = self._thdComp2
        data["thdComp3"] = self._thdComp3
        return data


    def _parseFromJsonData(self, data):
        super()._parseFromJsonData(data)
        self._filter = data["filter"]
        self._thdComp2 = data["thdComp2"]
        self._thdComp3 = data["thdComp3"]
