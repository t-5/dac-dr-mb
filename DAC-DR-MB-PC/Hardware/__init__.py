from .ES9038Q2M_sync import ES9038Q2M_sync
from .ES9038Q2M_async import ES9038Q2M_async
from .ES9039Q2M_sync import ES9039Q2M_sync
from .ES9039Q2M_async import ES9039Q2M_async

AVAILABLE_DAC_CLASSES = {
    "ES9038Q2M_sync": ES9038Q2M_sync,
    "ES9038Q2M_async": ES9038Q2M_async,
    "ES9039Q2M_sync": ES9039Q2M_sync,
    "ES9039Q2M_async": ES9039Q2M_async,
}
