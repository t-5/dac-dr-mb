from Hardware.AbstractES9039Q2M import AbstractES9039Q2M


class ES9039Q2M_async(AbstractES9039Q2M):


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._dpllBW = 15


    def init(self):
        """ send hardware initialization to DAC """
        if not self._active:
            return
        super().init()
        # disable sync mode
        #   register 1 SYS MODE CONFIG - bit 7 ENABLE_DAC_CLK, bit 6 SYNC_MODE, bit 0 ENABLE_TDM_DECODE
        self._writeI2cReg(1, 0b10000001)
        # set DPLL bandwidth
        #   register 29: DPLL BW - bit 7:4 DPLL_BW
        self._writeI2cReg(1, self._dpllBW << 4)
        # enable analog output
        #   register 0 - SYSTEM_CONFIG, bit 1: DAC_MODE
        self._writeI2cReg(0, 0b00000010)


    def settings(self) -> list:
        d = super().settings()
        d.append({
            "attribute": "_dpllBW",
            "description": "DPLL bandwidth",
            "type": int,
            "input": "int",
            "default": 5,
            "min": 1,
            "max": 15,
        })
        return d


    def _jsonData(self):
        data = super()._jsonData()
        data["dpllBW"] = self._dpllBW
        return data


    def _parseFromJsonData(self, data):
        super()._parseFromJsonData(data)
        self._dpllBW = data["dpllBW"]
