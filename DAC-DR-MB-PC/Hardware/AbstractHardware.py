from json import dumps, loads
from base64 import b64encode


class AbstractHardware:

    def __init__(self, serial, json=""):
        self._i2cChannel = 0 # cahnnels 1 to 7 are available
        self._active = True
        self._serial = serial
        if json:
            self._parseFromJson(json)


    def i2cAddr(self) -> int:
        raise NotImplemented


    def isActive(self):
        return self._active


    def json(self) -> str:
        return dumps(self._jsonData())


    def settings(self) -> list:
        return [
            {
                "attribute": "_active",
                "description": "Active",
                "type": bool,
                "input": "bool",
                "default": True,
            },
            {
                "attribute": "_i2cChannel",
                "description": "I2C/I2S Channel (1-7)",
                "type": int,
                "input": "int",
                "default": 1,
                "min": 1,
                "max": 7,
            },
        ]


    def _jsonData(self):
        return {
            "i2cChannel": self._i2cChannel,
            "active": self._active,
        }


    def _parseFromJson(self, json):
        data = loads(json)
        self._parseFromJsonData(data)


    def _parseFromJsonData(self, data):
        self._i2cChannel = data["i2cChannel"]
        self._active = data["active"]


    def _writeI2cReg(self, reg, value):
        i2cChans = b""
        for i in range(0, self._i2cChannel - 1):
            i2cChans += b"0"
        i2cChans += b"1"
        for i in range(self._i2cChannel, 8):
            i2cChans += b"0"
        cmd = b"<PC>i2cw" + i2cChans + b64encode(bytearray([self.i2cAddr(), reg, value])) + b"</PC>\n"
        self._serial.write(cmd)
