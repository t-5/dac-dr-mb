from Hardware.AbstractES9039Q2M import AbstractES9039Q2M


class ES9039Q2M_sync(AbstractES9039Q2M):


    def __init__(self, *args, **kwargs):
        super().__init__(*args, *kwargs)


    def init(self):
        """ send hardware initialization to DAC """
        if not self._active:
            return
        super().init()
        # enable sync mode
        #   register 1 SYS MODE CONFIG - bit 7 ENABLE_DAC_CLK, bit 6 SYNC_MODE, bit 0 ENABLE_TDM_DECODE
        self._writeI2cReg(1, 0b11000001)
        # enable analog output
        #   register 0 - SYSTEM_CONFIG, bit 1: DAC_MODE
        self._writeI2cReg(0, 0b00000010)
