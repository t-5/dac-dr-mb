from Hardware.AbstractES9039Q2M import AbstractES9039Q2M


class ES9038Q2M_async(AbstractES9039Q2M):


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._dpllBW = 15


    def init(self):
        """ send hardware initialization to DAC """
        if not self._active:
            return
        super().init()
        # set DPLL bandwidth
        #   Register 12: ASRC/DPLL Bandwidth, bits 7:4
        self._writeI2cReg(12, self._dpllBW)
        # set soft start ramp up of outputs (default: off)
        #   Register 14: Soft Start Configuration, bit 7 soft_start, bits 4:0 soft start time, default 0b1010
        self._writeI2cReg(7, 0b10001010)


    def settings(self) -> list:
        d = super().settings()
        d.append({
            "attribute": "_dpllBW",
            "description": "DPLL bandwidth",
            "type": int,
            "input": "int",
            "default": 5,
            "min": 1,
            "max": 15,
        })
        return d


    def _jsonData(self):
        data = super()._jsonData()
        data["dpllBW"] = self._dpllBW
        return data


    def _parseFromJsonData(self, data):
        super()._parseFromJsonData(data)
        self._dpllBW = data["dpllBW"]
