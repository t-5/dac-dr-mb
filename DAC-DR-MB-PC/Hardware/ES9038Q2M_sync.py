from Hardware.AbstractES9039Q2M import AbstractES9039Q2M


class ES9038Q2M_sync(AbstractES9039Q2M):


    def __init__(self, *args, **kwargs):
        super().__init__(*args, *kwargs)


    def init(self):
        """ send hardware initialization to DAC """
        if not self._active:
            return
        super().init()
        # enable sync mode
        #   Register 10: Master Mode and Sync Configuration, bit 4: 128fs_mode, bits 3:0 lock_speed (set to default)
        self._writeI2cReg(10, 0b00001010)
        # disable DPLL
        #   Register 12: ASRC/DPLL Bandwidth, bits 7:4
        self._writeI2cReg(12, 0b00001010)
        # disable DPLL
        #   Register 27: General Configuration, bit 7: asrc_en set to 0 => off,  bits 3:0 all of,
        #   => no volume sharing and no gain
        self._writeI2cReg(27, 0b00000000)
        # set soft start ramp up of outputs (default: off)
        #   Register 14: Soft Start Configuration, bit 7 soft_start, bits 4:0 soft start time, default 0b1010
        self._writeI2cReg(7, 0b10001010)
