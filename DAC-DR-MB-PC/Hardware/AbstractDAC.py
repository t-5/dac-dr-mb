from Hardware.AbstractHardware import AbstractHardware


class AbstractDAC(AbstractHardware):

    def __init__(self, *args, **kwargs):
        self._channel = "left"
        super().__init__(*args, **kwargs)


    def init(self):
        """ send hardware initialization to DAC """
        raise NotImplemented


    def settings(self) -> list:
        d = super().settings()
        d.append({
            "attribute": "_channel",
            "description": "Channel",
            "type": str,
            "input": "select",
            "options": {
                "left": "left",
                "right": "right",
            },
            "default": "left",
        })
        return d


    def setVolume(self, volume):
        """ send volume set command to DAC """
        raise NotImplemented


    def _jsonData(self):
        data = super()._jsonData()
        data["className"] = self.__class__.__name__
        data["channel"] = self._channel
        return data


    def _parseFromJsonData(self, data):
        super()._parseFromJsonData(data)
        self._channel = data["channel"]
