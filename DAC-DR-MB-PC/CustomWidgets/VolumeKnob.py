from math import pi, sin, cos

from PyQt5.QtCore import QRect, Qt, pyqtSignal
from PyQt5.QtGui import QColor, QConicalGradient, QPainter, QPen, QBrush, QPixmap, QFont
from PyQt5.QtWidgets import QWidget, QLabel


class VolumeKnob(QWidget):

	COLOR_00 = QColor(0, 255, 0)
	COLOR_05 = QColor(255, 255, 0)
	COLOR_10 = QColor(255, 0, 0)
	COLOR_BG = QColor(30, 30, 30)
	DRAG_SCALE = 0.25
	FONT_HEIGHT = 32
	LED_WIDTH = 10
	INSET_FACTOR = 0.2
	KNOB_SCALE = 0.85
	
	valueChanged = pyqtSignal(int)
	clicked = pyqtSignal()
	doubleClicked = pyqtSignal()

	def __init__(self, parent, width, steps=64, value=0, minValue=0):
		super().__init__(parent)
		# instance vars
		self._width = width
		self._steps = steps
		self._value = max(minValue, value)
		self._minValue = minValue
		# helper vars
		self._dragStartPosition = None
		self._dragStartValue = None
		# graphical knob label
		self._knobLabel = QLabel(self)
		self._knobLabel.setScaledContents(True)
		self._knobLabel.setPixmap(QPixmap(":/VolumeKnob/volume_knob.png"))
		self._knobLabel.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
		# led label
		self._ledLabel = QLabel(self)
		self._ledLabel.setScaledContents(True)
		self._ledLabel.setPixmap(QPixmap(":/VolumeKnob/led_green.png"))
		self._ledLabel.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
		self._ledLabel.setFixedSize(10, 10)
		# attenuation text label
		self._attenuationLabel = QLabel(self)
		self._attenuationLabel.setFont(QFont('DejaVu Sans', self.FONT_HEIGHT, QFont.Bold))
		self._attenuationLabel.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
		self._attenuationLabel.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
		self.setValue(value)


	def mouseDoubleClickEvent(self, event):
		# noinspection PyUnresolvedReferences
		self.doubleClicked.emit()


	def mouseMoveEvent(self, event):
		if self._dragStartPosition is not None:
			deltaPos = self._dragStartPosition - event.pos()
			self.setValue(int(round(self._dragStartValue + deltaPos.y() * self.DRAG_SCALE)), emitSignal=True)
			self.repaint()


	def mousePressEvent(self, event):
		if event.button() == Qt.LeftButton:
			self._dragStartPosition = event.pos()
			self._dragStartValue = self._value
			self.setCursor(Qt.SizeVerCursor)
		# noinspection PyUnresolvedReferences
		self.clicked.emit()


	def mouseReleaseEvent(self, event):
		self._dragStartPosition = None
		self._dragStartValue = None
		self.setCursor(Qt.ArrowCursor)


	def paintEvent(self, e):
		qp = QPainter()
		# noinspection PyTypeChecker
		qp.begin(self)
		qp.setRenderHint(QPainter.Antialiasing)
		# drawing rect
		drawingRect = QRect()
		drawingRect.setX(self.rect().x() + self._width)
		drawingRect.setY(self.rect().y() + self._width)
		drawingRect.setWidth(self.rect().width() - self._width * 2)
		drawingRect.setHeight(self.rect().height() - self._width * 2)
		# resize pixmap and attenuation label
		self._knobLabel.setFixedWidth(int(round(self.rect().width() * self.KNOB_SCALE)))
		self._knobLabel.setFixedHeight(int(round(self.rect().height() * self.KNOB_SCALE)))
		self._knobLabel.move(int(round(self.rect().width() * (1 - self.KNOB_SCALE) / 2)),
							 int(round(self.rect().height() * (1 - self.KNOB_SCALE) / 2)))
		self._attenuationLabel.setFixedSize(self.rect().width(), self.rect().height())
		self._attenuationLabel.move(0, 10)
		# setup gradient
		gradient = QConicalGradient()
		gradient.setCenter(drawingRect.center())
		gradient.setAngle(225)
		gradient.setColorAt(1.00, self.COLOR_00)
		gradient.setColorAt(0.63, self.COLOR_05)
		gradient.setColorAt(0.25, self.COLOR_10)
		gradient.setColorAt(0.15, self.COLOR_10)
		gradient.setColorAt(0.00, self.COLOR_00)
		# draw bg arc
		pen = QPen(QBrush(self.COLOR_BG), self._width + 2)
		pen.setCapStyle(Qt.RoundCap)
		qp.setPen(pen)
		qp.drawArc(drawingRect, 225 * 16, -270 * 16)
		# draw fg arc
		pen = QPen(QBrush(gradient), self._width)
		pen.setCapStyle(Qt.RoundCap)
		qp.setPen(pen)
		qp.drawArc(drawingRect,
				   225 * 16,
				   -int(round((270 * 16 / self._steps * self._value))))
		# draw led
		angle = -pi / 4 * 3 + (pi / 2 * 3 / self._steps * self._value)
		wy = self.rect().height() * (1 - self.INSET_FACTOR)
		wx = self.rect().width() * (1 - self.INSET_FACTOR)
		y_offset = int(round(wy / 2.4 * -cos(angle) - self.LED_WIDTH / 2))
		x_offset = int(round(wx / 2.4 * sin(angle) - self.LED_WIDTH / 2))
		self._ledLabel.move(self.rect().center().x() + x_offset,
						    self.rect().center().y() + y_offset)
		qp.end()


	def setMinValue(self, minValue):
		self._minValue = minValue


	def setValue(self, value, emitSignal=False):
		if value > self._steps:
			value = self._steps
		if self._minValue > value > 0:
			value = self._minValue
		if value < 0:
			value = 0
		if value == 0:
			self._attenuationLabel.setText("-∞ dB")
			self._ledLabel.setPixmap(QPixmap(":/VolumeKnob/led_red.png"))
		else:
			self._attenuationLabel.setText("-%d dB" % (self._steps - value))
			self._ledLabel.setPixmap(QPixmap(":/VolumeKnob/led_green.png"))
		self._value = value
		if emitSignal:
			# noinspection PyUnresolvedReferences
			self.valueChanged.emit(value)
		self.repaint()


	def value(self):
		return self._value
