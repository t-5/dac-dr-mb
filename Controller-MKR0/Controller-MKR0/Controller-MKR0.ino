#include <Scheduler.h>
#include <SoftWire.h>
#include <Base64.h>
#include <Rotary.h>


// defines
//#define DEBUG 1
#define MAX_I2C_BYTES 40
#define CMD_LEN 4
#define GPIO_NUM 16
#define I2C_BUSES 8
#define XRA1203_BUS 7 // the 8th bus
#define XRA1203_ADDR 0x40 // both addr pins tied to ground

// pin assignments
#define I2C1_SCL              25 // PA03
#define I2C1_SDA              15 // PA02
#define I2C2_SCL              16 // PB02
#define I2C2_SDA              17 // PB03
#define I2C3_SCL              18 // PA04
#define I2C3_SDA              19 // PA05
#define I2C4_SCL              20 // PA06
#define I2C4_SDA              21 // PA07
#define I2C5_SCL               0 // PA22
#define I2C5_SDA               1 // PA23
#define I2C6_SCL               2 // PA10
#define I2C6_SDA               3 // PA11
#define I2C7_SCL               4 // PB10
#define I2C7_SDA               5 // PB11
#define I2C8_SCL               7 // PA21
#define I2C8_SDA               6 // PA20
#define NOT_RESET             12 // PA09
#define ROTARY_ENCODER_CLK    11 // PA08
#define ROTARY_ENCODER_DATA   10 // PA19
#define ROTARY_ENCODER_BUTTON  9 // PA17
#define POWER_GOOD_INPUT       8 // PA16



// globals
// to blink or not to blink eagerly
uint8_t blink = 0;
// the serial parser's state
struct {
   char buf[64];
   int8_t offset;
   char cmd[55];
   uint8_t cmd_len;
   char client[2];
   char client_end[2];
   uint8_t found_end : 1;
   uint8_t found_end_dash : 1;
   uint8_t found_end_cl1 : 1;
   uint8_t found_end_cl2 : 1;
} serial;
// the 8 i2c buses
SoftWire* smbus[I2C_BUSES];
// the rotary encoder object
Rotary encoder(ROTARY_ENCODER_DATA, ROTARY_ENCODER_CLK);
// the rotary encoder's last button pressed state and the last power good state
uint8_t prev_encoder_button = HIGH;
uint8_t prev_power_good;
char swTxBuffer[16];
char swRxBuffer[16];


// main setup function
void setup() {
  // internal LED setup
  pinMode(LED_BUILTIN, OUTPUT);
  
  // init i2c interfaces
  smbus[0] = new SoftWire(I2C1_SDA, I2C1_SCL);
  smbus[1] = new SoftWire(I2C2_SDA, I2C2_SCL);
  smbus[2] = new SoftWire(I2C3_SDA, I2C3_SCL);
  smbus[3] = new SoftWire(I2C4_SDA, I2C4_SCL);
  smbus[4] = new SoftWire(I2C5_SDA, I2C5_SCL);
  smbus[5] = new SoftWire(I2C6_SDA, I2C6_SCL);
  smbus[6] = new SoftWire(I2C7_SDA, I2C7_SCL);
  smbus[7] = new SoftWire(I2C8_SDA, I2C8_SCL);
  for (uint8_t i = 0; i < 8; i++) {
    smbus[i]->setDelay_us(2);
    smbus[i]->setTimeout_ms(10);
    smbus[i]->setTxBuffer(swTxBuffer, sizeof(swTxBuffer));
    smbus[i]->setRxBuffer(swRxBuffer, sizeof(swRxBuffer));
    smbus[i]->begin();
  }

  // setup the XRA1203
  uint8_t cmdbuf1[3] = {XRA1203_ADDR, 0x06, 0b00000000};
  i2c_write(XRA1203_BUS, cmdbuf1, 2); // set gpios P0-P7 to output
  uint8_t cmdbuf2[3] = {XRA1203_ADDR, 0x07, 0b00000000};
  i2c_write(XRA1203_BUS, cmdbuf2, 2); // set gpios P8-P15 to output
  uint8_t cmdbuf3[3] = {XRA1203_ADDR, 0x0c, 0b00000000};
  i2c_write(XRA1203_BUS, cmdbuf3, 2); // disable three-state-mode for P0-P7
  uint8_t cmdbuf4[3] = {XRA1203_ADDR, 0x0d, 0b00000000};
  i2c_write(XRA1203_BUS, cmdbuf4, 2); // disable three-state-mode for P8-P15
  
  // serial init and thread
  uint8_t wait = 25;
  while (!Serial && wait > 0) {
    delay(100);
    wait--;
  }
  Serial.begin(152000);
  Serial.println("<MB>UP</MB>");
  serial_reset();
  serial.offset = 0;
  Scheduler.startLoop(loop_serial);

  // power good input init
  prev_power_good = 2; // must not be 1 or 0 in order to be different from any
                       // pin read value and trigger initial power good/bad status send
  pinMode(POWER_GOOD_INPUT, INPUT);

  // NOT_RESET pin setup
  pinMode(NOT_RESET, OUTPUT);

  // rotary enoder init
  pinMode(ROTARY_ENCODER_BUTTON, INPUT);
  encoder.begin(0, 1); // no pullups, inverted logic
  Scheduler.startLoop(loop_rotary_encoder);
}


// Main loop - blinks the led
void loop() {
  uint8_t counter = 100;
  if (blink) {
    delay(100);
    digitalWrite(LED_BUILTIN, LOW);
    delay(100);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN, LOW);
    delay(100);
    digitalWrite(LED_BUILTIN, HIGH);
    blink = 0;
  } else {
    while (!blink && counter > 0) {
      if (blink) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(20);
      } else {
        if (counter == 100) {
          digitalWrite(LED_BUILTIN, HIGH);
        } else if (counter == 50) {
          digitalWrite(LED_BUILTIN, LOW);
        }
        counter--;
        delay(20);
      }
    }
  }
}


// rotary encoder and power good loop
void loop_rotary_encoder() {
  uint8_t btn, pg, result;

  pg = digitalRead(POWER_GOOD_INPUT);
  if (pg != prev_power_good) {
    if (pg == HIGH) {
      Serial.println("<MB>POWER_GOOD</MB>");
    } else {
      Serial.println("<MB>POWER_BAD</MB>");
    }
    startBlink();
  }
  prev_power_good = pg;

  btn = digitalRead(ROTARY_ENCODER_BUTTON);
  if (btn == LOW && prev_encoder_button == HIGH) {
    Serial.println("<MB>MUTE</MB>");
    startBlink();
  }
  prev_encoder_button = btn;

  // only handle encoder input if button is not pressed
  result = encoder.process();
  if (result) {
    if (result == DIR_CW) {
      Serial.println("<MB>VOLUME_UP</MB>");
    } else if (result == DIR_CCW) {
      Serial.println("<MB>VOLUME_DOWN</MB>");
    }
    startBlink();
  }

  yield();
}


void serial_reset() {
  serial.offset = -1; // set offset to -1 because it will always get incremented at the end of the serial loop
  serial.cmd_len = 0;
  serial.found_end = 0;
  serial.found_end_dash = 0;
  serial.found_end_cl1 = 0;
  serial.found_end_cl2 = 0;
  #ifdef DEBUG
    Serial.println("SERIAL RESET");
  #endif
}


// tell the led state machine that it should blink (if not already blinking)
void startBlink() {
  if (!blink)
    blink = 1;
}


// carry out an i2c write with address in buf[0] and payload in the rest of buf.
// len must always be the actual payload's length.
uint8_t i2c_write(uint8_t bus, uint8_t buf[MAX_I2C_BYTES], uint8_t len) {
  uint8_t errors = 0;
  uint8_t success = 0;
  char errormsg[100];

  smbus[bus]->beginTransmission(buf[0] >> 1);
  for (uint8_t i = 1; i <= len; i++) { 
    success = smbus[bus]->write(buf[i]);
    if (!success) {
      sprintf(errormsg, "<MB>I2C_TX_ERROR_%u_0x%X_0x%X</MB>", bus, buf[0], buf[i]);
      Serial.println(errormsg);
      errors += 1;
    }
  }
  smbus[bus]->endTransmission();
  return errors;
}


// parse the already extracted serial command and execute it
void serial_parse_command() {
  char binbuf[MAX_I2C_BYTES], inbuf[MAX_I2C_BYTES + 1];
  uint8_t binlen, gpio0, gpio1, pg;
  
  #ifdef DEBUG
  Serial.write("CMD:");
  for (uint8_t i = 0; i < serial.cmd_len; i++) {
    Serial.write(serial.cmd[i]);
  }
  Serial.println("");
  #endif

  if (strncmp(serial.cmd, "gpio", CMD_LEN) == 0) {
    // gpio command. format: "gpioBBBBBBBBBBBBBBBB"
    // with B being either 0 or 1 for the respective GPIOs 0 to 15
    if (serial.cmd_len != CMD_LEN + GPIO_NUM) {
      Serial.println("<MB>GPIO_COMMAND_WRONG_LEN</MB>");
    } else {
      gpio0 = 0;
      gpio1 = 0;
      gpio0 |= (serial.cmd[4] == '1') << 0;
      gpio0 |= (serial.cmd[5] == '1') << 1;
      gpio0 |= (serial.cmd[6] == '1') << 2;
      gpio0 |= (serial.cmd[7] == '1') << 3;
      gpio0 |= (serial.cmd[8] == '1') << 4;
      gpio0 |= (serial.cmd[9] == '1') << 5;
      gpio0 |= (serial.cmd[10] == '1') << 6;
      gpio0 |= (serial.cmd[11] == '1') << 7;
      gpio1 |= (serial.cmd[12] == '1') << 0;
      gpio1 |= (serial.cmd[13] == '1') << 1;
      gpio1 |= (serial.cmd[14] == '1') << 2;
      gpio1 |= (serial.cmd[15] == '1') << 3;
      gpio1 |= (serial.cmd[16] == '1') << 4;
      gpio1 |= (serial.cmd[17] == '1') << 5;
      gpio1 |= (serial.cmd[18] == '1') << 6;
      gpio1 |= (serial.cmd[19] == '1') << 7;

      #ifdef DEBUG
        Serial.write("Set gpio  P0 to "); Serial.println( serial.cmd[4] == '1');
        Serial.write("Set gpio  P1 to "); Serial.println( serial.cmd[5] == '1');
        Serial.write("Set gpio  P2 to "); Serial.println( serial.cmd[6] == '1');
        Serial.write("Set gpio  P3 to "); Serial.println( serial.cmd[7] == '1');
        Serial.write("Set gpio  P4 to "); Serial.println( serial.cmd[8] == '1');
        Serial.write("Set gpio  P5 to "); Serial.println( serial.cmd[9] == '1');
        Serial.write("Set gpio  P6 to "); Serial.println(serial.cmd[10] == '1');
        Serial.write("Set gpio  P7 to "); Serial.println(serial.cmd[11] == '1');
        Serial.write("Set gpio  P8 to "); Serial.println(serial.cmd[12] == '1');
        Serial.write("Set gpio  P9 to "); Serial.println(serial.cmd[13] == '1');
        Serial.write("Set gpio P10 to "); Serial.println(serial.cmd[14] == '1');
        Serial.write("Set gpio P11 to "); Serial.println(serial.cmd[15] == '1');
        Serial.write("Set gpio P12 to "); Serial.println(serial.cmd[16] == '1');
        Serial.write("Set gpio P13 to "); Serial.println(serial.cmd[17] == '1');
        Serial.write("Set gpio P14 to "); Serial.println(serial.cmd[18] == '1');
        Serial.write("Set gpio P15 to "); Serial.println(serial.cmd[19] == '1');
      #endif
      uint8_t cmdbuf1[3] = {XRA1203_ADDR, 0x02, 0};
      uint8_t cmdbuf2[3] = {XRA1203_ADDR, 0x03, 0};
      cmdbuf1[2] = gpio0;
      cmdbuf2[2] = gpio1;
      i2c_write(XRA1203_BUS, cmdbuf1, 2);
      i2c_write(XRA1203_BUS, cmdbuf2, 2);
    }
  } else if (strncmp(serial.cmd, "i2cw", CMD_LEN) == 0) {
    // clear binbuf and inbuf
    for (uint8_t i = 0; i < (MAX_I2C_BYTES); i++) {
      binbuf[i] = 0;
    }
    for (uint8_t i = 0; i < (MAX_I2C_BYTES + 1); i++) {
      inbuf[i] = 0;
    }
    // copy serial.cmd[12:] to inbuf
    for (uint8_t i = 12; i < serial.cmd_len; i++) {
      inbuf[i - 12] = serial.cmd[i];
    }
    // decode binary string
    binlen = Base64.decodedLength(inbuf, serial.cmd_len - CMD_LEN - I2C_BUSES);
    Base64.decode(binbuf, inbuf, serial.cmd_len - CMD_LEN - I2C_BUSES);
    // send to respective i2c buses
    for (uint8_t i = 0; i < I2C_BUSES; i++) {
      if (serial.cmd[4 + i] == '1') {
        i2c_write(i, (uint8_t*)binbuf, binlen);
        #ifdef DEBUG
          Serial.write("Wrote to I2C #");
          Serial.write(i + 48); // convert int to ASCII
          Serial.write(": ");
          Serial.println(binbuf);
        #endif
      }
    }
  } else if (strncmp(serial.cmd, "pgst", CMD_LEN) == 0) {
    pg = digitalRead(POWER_GOOD_INPUT);
    if (pg == HIGH) {
      Serial.println("<MB>POWER_GOOD</MB>");
    } else {
      Serial.println("<MB>POWER_BAD</MB>");
    }
  } else if (strncmp(serial.cmd, "_rst0", CMD_LEN+1) == 0) {
    digitalWrite(NOT_RESET, LOW);
    #ifdef DEBUG
      Serial.println("Set NOT_RESET to 0.");
    # endif
  } else if (strncmp(serial.cmd, "_rst1", CMD_LEN+1) == 0) {
    digitalWrite(NOT_RESET, HIGH);
    #ifdef DEBUG
      Serial.println("Set NOT_RESET to 1.");    
    # endif
  } else if (strncmp(serial.cmd, "i2cr", CMD_LEN) == 0) {
    // TODO: send out i2c reads to designated ports
  } else {
    Serial.write("<MB>UNKNOWN_COMMAND|");
    Serial.write(serial.cmd);
    Serial.println("</MB>");
  }
}



// serial handler loop
void loop_serial() {
  char c;
  while (Serial.available()) {
    // handle potential buffer overflow case
    if (serial.offset >= 64) {
      Serial.println("<MB>PACKET_TOO_BIG</MB>");
      serial_reset();
    }
    // read char from serial
    c = serial.buf[serial.offset] = Serial.read();
    #ifdef DEBUG
      Serial.write("c: '");
      Serial.write(c);
      Serial.write("' serial.offset: ");
      Serial.println(serial.offset);
     #endif
    // handle char
    switch(serial.offset) {
      case 0:
        // check for start character
        if (c != '<') {
          // reset offset, we're ready to start anywhere in the stream
          serial_reset();
        }
        break;
      case 1:
        // parse client identifier first character (allowed: ASCII 0-9, A-Z)
        if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
          serial.client[0] = c;
        } else {
          Serial.println("<MB>INVALID_CHAR@1</MB>");
          serial_reset();
        }
        break;
      case 2:
        // parse client identifier second character (allowed: ASCII 0-9, A-Z)
        if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
          serial.client[1] = c;
        } else {
          Serial.println("<MB>INVALID_CHAR@2</MB>");
          serial_reset();
        }
        break;
      case 3:
        // check for start close character
        if (c != '>') {
          Serial.println("<MB>INVALID_CHAR@3</MB>");
          serial_reset();
        }
        break;
      default:
        if (!serial.found_end) {
          if (c == '<') {
            // start of end character
            serial.found_end = 1;
          } else {
            // character belongs to command - parse
            serial.cmd[serial.cmd_len] = c;
            serial.cmd_len++;
          }
        } else if (!serial.found_end_dash) {
          // parse end marker - dash
          if (c == '/') {
             serial.client_end[0] = c;
             serial.found_end_dash = 1;
          } else {
             Serial.println("<MB>INVALID_CLOSE_TAG_CHAR@0</MB>");
             serial_reset();
          }
        } else if (!serial.found_end_cl1) {
          // parse end client identifier first character (allowed: ASCII 0-9, A-Z)
          if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
             serial.client_end[0] = c;
             serial.found_end_cl1 = 1;
          } else {
             Serial.println("<MB>INVALID_CLOSE_CLIENT_CHAR@0</MB>");
             serial_reset();
          }
        } else if (!serial.found_end_cl2) {
          // parse end client identifier second character (allowed: ASCII 0-9, A-Z)
          if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
             serial.client_end[1] = c;
             serial.found_end_cl2 = 1;
          } else {
             Serial.println("<MB>INVALID_CLOSE_CLIENT_CHAR@1</MB>");
             serial_reset();
          }
        } else {
          // all end characters match - this must be the final closing >
          if (c == '>') {
            // check start and end client identifiers
            if (serial.client[0] != serial.client_end[0] || serial.client[1] != serial.client_end[1]) {
              Serial.println("<MB>CLIENT_ID_NOMATCH</MB>");
            } else {
              // all correct, end of command, parse command!
              #ifdef DEBUG
              Serial.write("cmd: ");
              Serial.println(serial.cmd);
              #endif
              serial_parse_command();
            }
          } else {
             Serial.println("<MB>INVALID_CLOSE_END_CHAR</MB>");
          }
          // reset in any case - failure or whatever...
          serial_reset();
        }
        break;
    }
    // handle blinking for serial rx
    startBlink();
    // increment serial offset
    serial.offset++;
  }
  yield();
}
